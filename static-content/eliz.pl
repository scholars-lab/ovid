#!/uva/bin/perl5

### eliz.pl ###

# Usage: eliz.pl [infile] >[outfile]


$openfile = $ARGV[0];

open( OPENFILE, "$openfile" );



while ( <OPENFILE> ) {

  print $_;

  if ( $_ =~ /page 144/ ) { # Changing this number changes the number
                          # *after* which it will start.
    switcher();

  }

}


sub switcher {

  while ( <OPENFILE> ) {

    $_ =~ /page ([0-9]+)/;

    $number = $1+2;       # The second number in this line is the number
                          # it will increment by.

    $_ =~ s/page ([0-9]+)/page $number/;

    print $_;

  }

exit;

}
