#!/usr/bin/perl5 -w

$dir = "/web/data/latin/ovid/spreng/";
opendir(DIR, $dir) or die "Can't open $dir: $!";

@files = grep (/.*\.html/,(readdir(DIR)));
closedir (DIR);


foreach $filename (@files) {

print "$filename\n";

open( OPENFILE, "$dir/$filename" );
open( OUTFILE, ">$dir/$filename.tmp" );


while ( <OPENFILE> ){

$_ =~ s/<\!--/<!--\n/g;
$_ =~ s/\/\/-->/\n\/\/-->/g;
$_ =~ s/{/\n{/g;
$_ =~ s/}/\n}/g;

print OUTFILE $_;

}

close OPENFILE;
close OUTFILE;
#system( "mv $dir/outfile $dir/$filename" );
#unlink ( "$dir/outfile" );


}
