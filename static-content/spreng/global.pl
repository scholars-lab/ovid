#!/usr/bin/perl5 -w

$dir = "/web/data/latin/ovid/spreng/";
opendir(DIR, $dir) or die "Can't open $dir: $!";

@files = grep (/.*\.html/,(readdir(DIR)));
closedir (DIR);


foreach $filename (@files) {

print "$filename\n";

open( OPENFILE, "$dir/$filename" );
open( OUTFILE, ">$dir/$filename.tmp" );

undef $/;

while ( <OPENFILE> ){

$_ =~ s/<\!--\s*\n*function\s*newwindow3.*?Virgil Solis<\/a>\s*\(1563\)<br><br>/
<!--function newwindow(windowcontents,windowname) {\n
windowname=window\.open(windowcontents,windowname,"width=370,toolbar=yes,location=yes,menubar=yes,resizable=1,scrollbars=yes,status=yes")\n
windowname\.focus()}\/\/-->\n
<!--function newwindow3(windowcontents,windowname) {\n
windowname=window.open(windowcontents,windowname,"width=650,toolbar=no,location=yes,menubar=yes,toolbar=yes,resizable=1,scrollbars=yes,status=yes")\n
windowname.focus()}\/\/-->\n
<\/SCRIPT><\/head>\n
<body background="http:\/\/etext.lib.virginia.edu\/eaf\/images\/eafback.jpg">\n
<center><b><a href="javascript:newwindow('http:\/\/etext.virginia.edu\/latin\/ovid\/ovid1563.html','window10')"><font color="brown">\n
Ovid Illustrated: The Renaissance Reception of Ovid in Image and\n
Text<\/a><\/font><br><font size="-1">J. Spreng's <i>Metamorphoses Illustratae<\/i>, ill. <a href="javascript:newwindow('http:\/\/etext.virginia.edu\/latin\/ovid\/abouttext.html','window08')">Virgil Solis<\/a> (1563) -- \n
<a href="javascript:newwindow('http:\/\/etext.virginia.edu\/latin\/ovid\/ovid1563l.html','window21')">largest-format<\/a>  \/\/ <a href="javascript:newwindow('http:\/\/etext.virginia.edu\/latin\/ovid\/1612.html','window37')">later woodcut state<\/a><br><br>/sg;

print OUTFILE $_;

}

close OPENFILE;
close OUTFILE;
#system( "mv $dir/outfile $dir/$filename" );
#unlink ( "$dir/outfile" );


}
