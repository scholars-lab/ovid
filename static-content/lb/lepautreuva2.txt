Call number:  Micfilm 1254 Reel 32
Author: Le Pautre, Jean, 1618-1682.
Title: Pautre's designs [microform] : 22 sets of engraved plates / by Jean Le Pautre.
Publication info: [Paris : P. Mariette [etc.], [16--?]
Description: [137] leaves of plates (some folded) : all ill. ; 32 cm.
Note: Binder's title.
Note: Six of the sets of plates show evidence of the erasure of the imprint of Nicholas Langlois and the substitution of Mariette's name and address on the title pages and on the plates.
Note: For a detailed listing of the sets of plates see the Fowler cat. no. 182, no. 1-22. (Johns Hopkins University. John Work Garrett Library. The Fowler architectural collection. Baltimore, 1961)
Reproduction note: Microfilm. New Haven : Research Publications, [1979]. 1 microfilm reel ; 35 mm. -- (Fowler collection of early architectural books ; reel 32, no. 182)
Subject: Architecture--Details.
Subject: Decoration and ornament, Architectural.
Series: Fowler collection of early architectural books ; no. 182
[UVa 1 supposedly includes 253 plates]
