<table width=100%>
<tr>
<td align=center><IMG SRC="http://ovid.lib.virginia.edu/OW.jpg" alt="Renaissance
Painting"></td>
<td align=center>


<font color="brown" size="+2">
<i>Ovidii Metamorphoses Illustratae</i></font>
<br><br>
Illustrations of Ovid's
<i>Metamorphoses</i> by Virgil Solis 
<br>
with a verse commentary by Johann Spreng, 1563
<br>

</td>
<td align=center><IMG src="http://ovid.lib.virginia.edu/toolbar.gif" usemap="#toolbar" border="0"></td>
</tr>
</table>

<map name="toolbar">
<area shape="rect" alt="Electronic Text Center" coords="0,0,105,25" href="http://www2.lib.virginia.edu/etext/index.html" title="Electronic Text Center">
<area shape="rect" alt="Ovid Collection Home Page" coords="1,32,105,59" href="http://ovid.lib.virginia.edu/index.html" title="Ovid Collection Home Page">
<area shape="rect" alt="English Translation" coords="2,68,106,90" href="http://etext.lib.virginia.edu/etcbin/toccer-new?id=OviEMet&tag=public&images=images/modeng&data=/texts/english/modeng/parsed&part=0" title="English Translation">
<area shape="rect" alt="Latin Text" coords="0,99,105,123" href="http://etext.lib.virginia.edu/etcbin/toccer-new?id=OviLMet&tag=public&images=images/modeng&data=/texts/latin&part=0" title="Latin Text">
<area shape="rect" alt="Search" coords="1,131,105,156" href="http://etext.virginia.edu/etcbin/ot2www-pubeng?specfile=/web/data/latin/ovid/search/ovidmet.o2w" title="Search">
<area shape="default" nohref>
</map>




<hr width=80%>
