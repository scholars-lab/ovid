#!/usr/local/bin/perl5 

#####################################################
#
#  File:   ovid-pageturner2.pl
#  Desc:  
#
#  Last Modified:  04/02/02 -	Sue Munson
#				sjm8k@virginia.edu
#####################################################

require "1563.inc";

$ENV{'PATH'} = '/bin:/usr/bin:/usr/local/bin';
BEGIN {
   unshift (@INC, '/www/cgi-local/ovid');
}

use CGI qw(:standard);
$query = new CGI;

print $query->header;
print $query->start_html(-title=>'Browse Spreng/Ovid1563 by Page Images -- University of Virginia Library',
                         -author=>'sjm8k@virginia.edu',
                         -background=>'http://ovid.lib.virginia.edu/images/parch.jpg',
                         -text=>'#000000',
                         -link=>'#3366BB',
                         -alink=>'#FF0000',
                         -vlink=>'#800000');

# Main

# IF FIRST TIME GET THE PARAMETER VALUES FROM THE URL ($id,$image) ELSE GET FROM HIDDEN FIELDS ($volume,$jpeg)
# PASS TO SUBS AS $vol and $jpg
   
   if ($query->param('jpeg') eq "") {
      $jpg = $query->param('image');
   } else {
      $jpg = $query->param('jpeg');
   }

   $cnt = @pkeys;
   
   if ($query->param('forward.x') ne "") {
      for($i=0; $i<$cnt; $i++) {
         if ($pkeys[$i] eq $jpg) {
            ++$i;
            if ($i >= $cnt) {
               print "<BR><CENTER><H3>End of Book</H3></CENTER><BR>";
            } else {
               $jpg = $pkeys[$i];
            }
         }
      }
   }

   if ($query->param('back.x') ne "") {
      for($i=0; $i<$cnt; $i++) {
         if ($pkeys[$i] eq $jpg) {
            --$i;
            if ($i== -1) {
               print "<BR><CENTER><H3>Start of Book</H3></CENTER><BR>";
               last;
            } else {
               $jpg = $pkeys[$i];
            }
         }
      }
   }

   $query->param(-name=>'jpeg',-value=>"$jpg");
   $jpg2 = $pages{$jpg};
   &display_form($jpg,$jpg2);
   print $query->end_html;

sub display_form {
   my($jpg,$jpg2) = @_;
   print $query->startform;
   print $query->hidden(-name=>'jpeg',-default=>"$jpg");
   print "<CENTER>\n";
   print $query->image_button(-name=>"back",
                              -src=>"$icondir/lhand.gif");

   print "&nbsp;&nbsp;&nbsp;\n";
   print "<FONT SIZE = \"-1\" COLOR=\"brown\"><B><a href=\"http://ovid.lib.virginia.edu/ovid1563.html\">";
   print "<I>Ovidii Metamorphoses Illustratae, 1563</I></A></FONT></B>\n";
   print "&nbsp;&nbsp;&nbsp;\n";
   print $query->image_button(-name=>"forward",
                              -src=>"$icondir/rhand.gif");
   print "<BR>\n";
   if ($jpg =~ /spceyx/) {
      print "<a href=\"$imagedir/medium/sprengceyxa.jpg\" target=\"_blank\"><img src=\"$imagedir/NEW/$jpg\"></a>\n";
      print "<a href=\"$imagedir/medium/sprengceyxb.jpg\" target=\"_blank\"><img src=\"$imagedir/NEW/$jpg2\"></a>\n";
      
      print "<BR><BR><FONT SIZE = \"-2\">\n";
      print "<B>Note: </B>missing leaf here supplied from the Biblioth&egrave;que Nationale copy of \n";
      print "<B><FONT COLOR=\"brown\">\n";
      print "<A HREF=\"http://ovid.lib.virginia.edu/abouttext.html#Spreng1570\">Paris, 1570</A></B></FONT>\n";
      print "<BR>Click <B><FONT COLOR=\"brown\">\n";
      print "<A HREF=\"http://ovid.lib.virginia.edu/ceyx.jpg\">here</A></B></FONT>\n";
      print "for the missing Solis woodcut, supplied from the BN copy of Posthius' \n";
      print "<B><FONT COLOR=\"brown\">\n";
      print "<A HREF=\"http://ovid.lib.virginia.edu/abouttext.html#outside\"><i>Tetrasticha</i></A>\n";
      print "</B></FONT> (Frankfurt, 1569)</FONT>\n";
   } else {
      print "<a href=\"$imagedir/medium/$jpg\" target=\"_blank\"><img src=\"$imagedir/NEW/$jpg\"></a>";
      print "<a href=\"$imagedir/medium/$jpg2\" target=\"_blank\"><img src=\"$imagedir/NEW/$jpg2\"></a>";
   }
   print "</CENTER>\n";
   print $query->endform; 
}
