#!/usr/local/bin/perl5 -Tw

$ENV{'PATH'} = '/bin:/usr/bin:/usr/local/bin';

BEGIN {
   unshift (@INC, '/www/cgi-local');
}

#####################################################
#
#  File:   ovid-pageturner.pl
#  Desc:  
#
#  Last Modified:  03/12/99 -	Sue Munson
#				sjm8k@virginia.edu
#####################################################
# Globals
$imagedir = "http://ovid.lib.virginia.edu/images/";	# to get jpgs - /www/data/eaf/texts/images
$icondir = "http://etext.virginia.edu/eaf/miscimages/";	# to get finger icons
$headdir = "/www/data/latin/ovid";			# to get html file for header creation
#####################################################

use CGI;
$query = new CGI;

print $query->header;
print $query->start_html(-'title'=>'Browse OVID by Page Images -- University of Virginia Library',
                         -'author'=>'sjm8k@virginia.edu',
                         -'background'=>'http://etext.lib.virginia.edu/eaf/images/eafback.jpg',
                         -'text'=>'#000000',
                         -'link'=>'#3366BB',
                         -'alink'=>'#FF0000',
                         -'vlink'=>'#800000');

# Main
$my_self = $query->url;

# IF FIRST TIME GET THE PARAMETER VALUES FROM THE URL ($id,$image) ELSE GET FROM HIDDEN FIELDS ($volume,$jpeg)
# PASS TO SUBS AS $vol and $jpg
   
   if ($query->param('jpeg') eq "") {
      $jpg = $query->param('image');
   } else {
      $jpg = $query->param('jpeg');
   }
   if ($query->param('submit') eq "Go to Start of Book") {
       $jpg = "OVIM009.jpg";
       $query->param(-name=>'jpeg',value=>"$jpg");
   } elsif (($query->param('right.x') ne "") || ($query->param('left.x') ne "")) {
       $jpg = &find_next_page($jpg);
   } 
   &display_form($jpg);
   &footer(); 
   print $query->end_html;

sub display_form {
   local($jpg) = $_[0];
   my($jpgfile) = "";

   print $query->startform;
# PASS VALUES
   print $query->hidden(-name=>'jpeg',-default=>"$jpg");

# SETUP JPEG FILE PATH - FIND OUT WHAT SIZE JPG USER WANTS OR DEFAULT TO SMALL IF FIRST TIME 
   $which_radio_button = $query->param('jpgsize');
   if ($which_radio_button eq "") {
      $jpgfile = "$jpg";
   } else {
      $jpgfile = "medium/$jpg";
   }

   print "<CENTER>\n";

   print "<TABLE><TR><TD>\n";
   print $query->image_button(-name=>'left',
                              -src=>"$icondir/lhand.gif",
                              -align=>left);
print "&nbsp;&nbsp;&nbsp;</TD><TD>\n";  
print $query->radio_group(-name=>'jpgsize',
                             -values=>['','medium'],
                             -default=>'',
                             -labels=>{''=>' small','medium'=>' large'});

   print "</TD><TD>\n";
   if ($query->param('submit') ne "Go to Page") {
      $query->param(-name=>'num',-value=>"");
   }
   print $query->image_button(-name=>'right',
                              -src=>"$icondir/rhand.gif",
                              -align=>right);
   print "</TD></TR></TABLE>\n";

  print "<CENTER><FONT SIZE = \"-1\" COLOR=\"brown\"><B><a href=\"http://ovid.lib.virginia.edu/ovid1563.html\">
<I>Ovidii Metamorphoses Illustratae, 1563</I></A></FONT></B></CENTER>\n";
   print "<img src=\"$imagedir$jpgfile\">";
   print $query->endform; 
}

##########
# This subroutine reads the html file in order to 
# create the header for the image page.  The code
# saves the first jpg filename in the html file 
# and returns it for first page reference.
##########

sub get_bio {
   my($fileid) = $_[0];
   my($filedir) = $_[1];
   my($start_printing) = "";
   open(FILE, "$headdir/$filedir/$fileid.html/") || die "Can't find html file\n";
   while(<FILE>) {
      if (/pageturner\.pl\?id=eaf\w+\&image=(.*\.jpg)/i) {
         $fp=$1;
         last;
      }
      if (/<h3>Browse Page Images/i) {
         $start_printing = 1;
         print "<CENTER>\n";
      } 
      if ($start_printing == 1) {
         if ((/<table/i) || (/<tr/i) || (/<td/i) || (/<center/i)) {
           next;
         } else {
            print "$_";
         }
      }
   }
   close(FILE);
   return($fp);
}

# Very important subroutine -- get rid of all the naughty
# metacharacters from the input field.  IF there are, we 
# complain bitterly and die.

sub validate {
   local($ret) = "booboo";
   local($num) = $query->param('num');
   if (($num =~ /\W+/) || ($num eq "")) {
      print "<H2><STRONG>Are you sure you entered a valid page number?  ",
      "Only digits and roman numerals are accepted.</STRONG></H2>";
      return($ret);
   }
}

sub find_next_page {
   my($jpg) = $_[0];

   my(@a,@b,@a1,@a2,%pagenums,$k,$v,$cnt,$i,$lastnum,$tmp_a1,$tmp_a2);

$/="";
   open(READ, "$headdir/ovid1563page.html");
   while(<READ>) {
      if (/pageturner\.pl/) {
         @a = /image=(OVIM\w+\.jpg)">/g;
         foreach $tmp_a1 (@a) {
            push(@a1,$tmp_a1);
         }
         @b = /\.jpg">(.*)<\/a>/g;
         foreach $tmp_a2 (@b) {
            push(@a2,$tmp_a2);
         }
      }
   }
   close(READ);


## BUILD HASH
   $cnt = @a1;
   for($i=0; $i<$cnt; $i++) {
      $k = $a1[$i];
      $v = $a2[$i];
      $pagenums{$k}=$v;
      if ($k =~ /\d+/) {
         $lastnum = $v;
      }
   }

## IF RIGHT FINGER
   $cnt = @a1;
   if ($query->param('right.x') ne "") {
      for($i=0; $i<$cnt; $i++) {
         if ($a1[$i] eq $jpg) {
            ++$i;
            if ($i >= $cnt) {
               print "<BR><CENTER><H3>End of Book</H3></CENTER><BR>";
            } else {
               $jpg = $a1[$i];
            }
         }
      }
   }

## IF LEFT FINGER
   
   if ($query->param('left.x') ne "") {
      for($i=0; $i<$cnt; $i++) {
         if ($a1[$i] eq $jpg) {
            --$i;
            if ($i== -1) {
               print "<BR><CENTER><H3>Start of Book</H3></CENTER><BR>";
               last;
            } else {
               $jpg = $a1[$i];
            }
         }
      }
   }

## IF SUBMIT PAGENUMBER
   $found = 0;
   if ($query->param('submit') eq "Go to Page") {
      while(($a,$b) = each(%pagenums)) {
         if ($b =~ /page\s*(\d+)/i) {
            $b = $1;
            $b =~ s/^0+//;
            $ba = $b . "a";
         }
         unless ($query->param('num') =~ /b/) {    
            if (($b eq $query->param('num')) || ($ba eq $query->param('num'))) {
                $jpg = $a;
                $found = 1;
                last;
            }
         }   
      }
      if ($found != 1) {
         print "<BR><CENTER><H3>Sorry.  This page does not exist</H3></CENTER><BR>";
      }
   }
   $query->param(-name=>'jpeg',-value=>"$jpg");
   return($jpg);
}

sub footer {
open (FILE, "uvafoot.txt") || die "Can't find text file\n";
 while(<FILE>) {
    print $_;
 }
close(FILE);
}
