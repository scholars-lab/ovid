1 about
2 1557
3 1563
4 1582
5 1591
6 posthius1-7,8-15
7 tempestabaurnew
8 notes
9 dePasseNew
10 ovid1563
11 gryphius
12 lyon742
13 paris137
14 dolce,banierfrench book by book
15 krauss
16 brooks,paris137index
17 anguillara
18 sandys
19 garth
20n,21,24,29n,37,40n,42,80n98n,,99n incidental entries (the last only in "Notes" and "Portraits")  n=new as of 3/3/06
[20: 1484, Fontanelle1706, 1787; 21: 1498, 1698index, 1703, 1799, 1958; 24: 1497, 1676, 1717, 1720index, 1931a; 29: 1501, 1557 (cf. 1584), 1612 (cf. 1563), metamorphosesdovide; 37: 1526, 1717 (JD), 1757, 1732, 1824/32index, 1931b, 1946; 40: 1540; 42: 1545/51, 1720, 1771; 80: 1681, 1720bwoverview, 1801; 98: 1655/1732 (tdm),;]
21: 1498, 1698index, 1703, 1799, 1958, missing pages in banierfr
22 salomonsimeoni
23 kline trans (links can shift to Latin directly)
24: 1497, 1676, 1717, 1720index, 1937a
25 sandystableaux (sandys int.)
26 sandyssidenotes
27 sandyscommentary
28 sandyscomm. notes
29: 1501, 1557 (cf. 1584), 1612 (cf. 1563), metamorphosesdovide
30 1584
31 others
32 latin1904
33 tempesta
34 baur, 1698
35 depasse
36 goltzius
37: 1526, 1717 (JD), 1757, 1732, 1824/32, 1931b, 1946
38 lepautre
39 carte color overview
40: 1540,histoirepoetique
41 largest-format 1563, Du Ryer text
42: 1545/51, 1720, 1771
43 rubens,desaintange,tdm1655,googlebooks
44 goethals 1702,gros
45 gottfried 1619
46 renouard 1637
47 lemirebasan1770
48 banier 1732, Venice 1555
49 golding, 1697e
50 narrationes, DuRyer 1702
51 berchorius, DuRyer 1728
52 lyonindex
53 cyclesintableform
54 dolceindex,burmann
55 renouardindex
56 lemirebasan1770index
57 brooksindex, Fontanelle1802
58 lebrunleclercindex
59 latinroster
60 bibliothecaaugustanalatin, DuRyer
61 beroaldo1518
77 search
78 home
79 vignette1563,picart1732onebyone
80: 1681, 1720bwoverview, 1801
81 <<kline index (links shift to Kline translation directly)>>
83 villenave index
84 villenave
85 1697r
86 garth index,tdm1732
87 garth book by book,tdm1688
88 baniernotes
90 klinenotes
99 1824/32
