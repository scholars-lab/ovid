#! /usr/bin/perl5 -w

#################################################################################################
#												#
#	This script takes in one .txt file with a bunch of htmls cated together, 		#
#	and splits it up into a series of smaller html files based on the close </html> tag. 	#
#												#
#	If there are questions, contact Matthew Gibson at the Electronic Text Center,		#
#	University of Virginia Library (mgibson@virginia.edu)					#
#												#
#	Copyright, Rector & Board of Visitors, University of Virginia				#
#												#
#################################################################################################

$filename = $ARGV[0];

open( OPENFILE, "$filename" || die "OPEN A FILENAME!\n\n");
#open( OUTFILE, ">$filename.html" );

$/="</html>";  ### split on close htmls in larger file.txt or whatever has a bunch of htmls in it 

while (<OPENFILE>) 
{

        $a = $_;


	$a =~  /<img src="small\/(.*?)\.jpg">/ && ($file=$1);

	$filename = $file;
	print "$filename\n";
	open(OUTFILE, ">$filename.html") || die "can't open outfile";

	print OUTFILE $a;
	#print OUTFILE "</html>";
}

close OUTFILE;
close OPENFILE;






