#!/usr/local/bin/perl

$filename = $ARGV[0];

open (OPENFILE, "$filename");
open (OUTFILE, ">$filename.tmp");

while (<OPENFILE>) {

$_ =~ s/ovid1563\.html/ovidillust\.html/g;
$_ =~ s/abouttext\.html/about\.html/g;
$_ =~ s/search\.html/search\.html\#search/g;

print OUTFILE $_;

}
close OPENFILE;
close OUTFILE;
