# Colonial project archive
Static generated files from http://ovid.lib.virginia.edu

## Process of creating the static archive
- Scraping the old site was done with this command:
  - `wget --mirror -P static-content -nH -np -p -k -E http://ovid.lib.virginia.edu/`
- All files were archived in a bzip2.tar file using this command:
  - `tar -jcf ovid.bzip2.tar releases/20120820141539`
- To recreate the site
  - Put the contents of the 'static-content' folder on a web server. It is just static HTML files.
  - Or use Docker to build an nginx container with the folder 'static-content' linked to the folder /usr/share/nginx/html in the container.
    - The docker-compose.yml file needs to have the static-content folder on the same level as itself.
    - The docker-compose.yml file requires Traefik to be running and a 'thenetwork' docker network to be created. See here: https://github.com/scholarslab/traefik

## File Structure
```
/static-content = contains all of the site's files. Copied directly from the old
                 site on the SDSV servers
docker-compose.yml = file used to run the site as a Docker container
```

